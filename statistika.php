<?php

require_once "dblink.php";

class statistika {

 //TA SPREMENLJIVKA VSEBUJE ŠTEVILO RAČUNALNIKOV ODDANIH NA DOLOČEN DAN
 public $raw;

 function __construct($id_statistika = '1', $izjema = "") {

  //BAZO POTREBUJEMO LE TEKOM TE FUNKCIJE
  $db = new dblink();

  //PRIDOBI PODATKE
  if($id_statistika == "1" || $id_statistika == "8") {
   $vrste = "'1','2'";
  } else {
   $vrste = "'".$db->e($id_statistika)."'";
  }


  $vnosi = $db->q("
   SELECT SUM(`stevilo`),`datum` FROM `t_statistika_oprema`
    WHERE `id_statistika` = '".$db->e($id_statistika)."'
      AND `id_vrsta` IN (".$vrste.")
    ".$izjema."
    GROUP BY `datum`
    ORDER BY `datum` ASC
  ");

  $this->raw = array();
  foreach($vnosi as $vnos) {

   $this->raw[strtotime($vnos["datum"])] = ((int) $vnos["SUM(`stevilo`)"]);

  }

 }

 function faktor($faktor = 1) {
  $old = $this->raw;
  foreach($old as $i => $v) {
   $this->raw[$i] = $v * $faktor; 
  }
 }

 function vsiDnevi($od = "2011-01-01") {
  //OKRAJŠAJ NA CELE DNI
  $od = date("Y-m-d",strtotime($od));

  //NORMALIZIRAJ PODATKE (progresivno seštevaj po dnevih ki imajo podatke)
  $sum = 0; $output = array();
  for ($t = strtotime($od); $t < time(); $t = strtotime("+1 day", $t)) {
   if(isset($this->raw[$t])) {
    $sum = $sum + $this->raw[$t];
   }
   $output[$t] = $sum;
  }

  return $output;

 }

 function vsiMeseci($od = "2011-01-01") {

  //OKRAJŠAJ NA CELE MESECE
  $od = date("Y-m",strtotime($od))."-01";

  //PRIDOBI PODATKE
  $dnevi = $this->vsiDnevi($od);
 
  //POBERI SEŠTEVEK PO MESECIH (zadnji dan v mesecu)
  foreach($dnevi as $datum => $stevilo) {
   $mesec = strtotime(date("Y-m-t",$datum));
   if($datum == $mesec) {
    $output[$mesec] = $stevilo;
   }
  } 

  return $output;

 }

 function __destruct() {
  unset($this->raw);
 }

}
