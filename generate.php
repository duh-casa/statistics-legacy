#!/usr/bin/php
<?php

//DEFINIRAJ POTI
$rrdFile = "racunalniki.rrd";

require_once "statistika.php";
$s = new statistika();

require_once "graf.php";
$graf = new graf($rrdFile);
$graf->dodajTocke($s->vsiDnevi());
$graf->izrisi("graf", "FF0000", "Oddanih računalnikov");
