<?php

 class graf {

  private $rrdFile;

  function __construct($rrdFile = "example.rrd") {

   $this->rrdFile = __DIR__."/dynamic/".$rrdFile;

   //IZBRISI STARO RRD BAZO
   if(file_exists($this->rrdFile)) {
    unlink($this->rrdFile);
   }

   //USTVARI RRD BAZO
   $data = array("--start","1291075200","DS:tocke:GAUGE:86400:U:U","RRA:AVERAGE:0.99:3:86400");
   rrd_create($this->rrdFile, $data);

  } 

  function dodajTocke($tocke = array()) {

   //IZGRADI RRD ZAPISE
   $data = array();
   foreach ($tocke as $cas => $tocka) {
    $data[] = $cas.":".$tocka;
   }

   rrd_update($this->rrdFile,$data);
   echo rrd_error();

  }

  function izrisi($kaj = "", $barva = "FF0000", $opis = "Example") {

   //IZRISI GRAFE
   $graf = __DIR__."/dynamic/".strtolower($kaj)."-leto.png";
   $data = array(
    "--start","-1y",
    "--end","now",
    "--vertical-label", trim($kaj, "-"),
    "DEF:rac=".$this->rrdFile.":tocke:AVERAGE",
    'LINE2:rac#'.$barva.':"'.$opis.'"'
   );

   rrd_graph($graf, $data);
   print_r(rrd_error());

   $graf = __DIR__."/dynamic/".strtolower($kaj)."-mesec.png";
   $data = array(
    "--start","-1m",
    "--end","now",
    "--vertical-label", trim($kaj,"-"),
    "DEF:rac=".$this->rrdFile.":tocke:AVERAGE",
    'LINE2:rac#'.$barva.':"'.$opis.'"'
   );

   rrd_graph($graf, $data);

   $graf = __DIR__."/dynamic/".strtolower($kaj)."-teden.png";
   $data = array(
    "--start","-1w",
    "--end","now",
    "--vertical-label", trim($kaj,"-"),
    "DEF:rac=".$this->rrdFile.":tocke:AVERAGE",
    'LINE2:rac#'.$barva.':"'.$opis.'"'
   );

   rrd_graph($graf, $data);

  } 

 }
